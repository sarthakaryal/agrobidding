package com.demo.Controller;






import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.demo.Repository.FarmerRepository;
import com.demo.Repository.ProductRepository;
import com.demo.model.Farmer;
import com.demo.model.Product;

import jakarta.servlet.http.HttpSession;




@Controller
public class farmerController {
	
	@Autowired
	private FarmerRepository farmRepo;
	
	@Autowired
	private ProductRepository producrRepo;
	
	@GetMapping("/farmer")
	public String retailerLogin() {
		
		return"farmerLogin";
	}
	
	@GetMapping("/signup")
	public String retailerSignup() {
		
		return"farmerSignup";
	}
	
	@PostMapping("/fsignup")
	public String saveFarmer(@ModelAttribute Farmer farmer) {
		farmRepo.save(farmer);
		return"farmerLogin";
	}
	
	

		@PostMapping("/flogin")
	public String doLogin(@ModelAttribute Farmer farmer, Model model, HttpSession session) {
		Farmer farm =farmRepo.findByEmailAndCitizenshipnumber(farmer.getEmail(),farmer.getCitizenshipnumber()); 
		if (farm!=null) {
			session.setAttribute("farmer", farm);
			session.setMaxInactiveInterval(120);
			model.addAttribute("uname",farmer.getCitizenshipnumber());
			return"bootstrapheader";
		}else {
			model.addAttribute("message","user not found!!");
		return"farmerLogin";
		}
	}		
	
	@GetMapping("/edit")
	   public String editProduct(@RequestParam Integer id,Model model) {
		   
	    	model.addAttribute("emodel",producrRepo.findById(id));
		   return"editForm";
	   }
	    @PostMapping("/update")
	    public String updateProduct(@ModelAttribute Product product) {
	    	
	    	producrRepo.save(product);
	    	
	    	return"redirect:/list";
	    }
	    
	    @GetMapping("/delete")
	    public String deleteProduct(@RequestParam Integer id) {
	        producrRepo.deleteById(id);
	        return "redirect:list";
	    }
	    
	   
	  

}
