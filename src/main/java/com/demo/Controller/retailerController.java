package com.demo.Controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.demo.Repository.RetailerRepository;
import com.demo.model.Farmer;
import com.demo.model.Product;
import com.demo.model.Retailer;

import com.demo.utils.VerifyRecaptcha;

@Controller
public class retailerController {
	
	@Autowired
	private RetailerRepository retailerRepo;
	
	
	@GetMapping("/retailer")
	public String showloginR() {
		
		return"retailerLogin";
	}
	
	
	@GetMapping("/rsignup")
	public String doLoginR() {
		
		return"retailerSignup";
	}
	
	@GetMapping("/rlogin")
	public String showLogin() {
		
		return"retailerLogin";
	}
	
	
	@PostMapping("/rlogin")
	public String doLogin(@ModelAttribute Retailer retailer,Model model,@RequestParam("g-recaptcha-response") String reCode )throws IOException {
	retailer.setPassword(DigestUtils.md5DigestAsHex(retailer.getPassword().getBytes()));
		
	if(	VerifyRecaptcha.verify(reCode)) {
	Retailer retailers =retailerRepo.findByUsernameAndPassword(retailer.getUsername(),retailer.getPassword()); 

		if(retailers!=null) {
			
		model.addAttribute("uname",retailer.getUsername());
			return"retailerdashboard";
		}else {
		model.addAttribute("message","user not found!!");
		return"retailerLogin";
		}
	}
		model.addAttribute("message","Are you a robot!!");
		return "retailerlogin";
	}
	

	


	@PostMapping("/rsignup")
	public String doSignupRetailer(@ModelAttribute Retailer retailer) {
		retailer.setPassword(DigestUtils.md5DigestAsHex(retailer.getPassword().getBytes()));
		retailerRepo.save(retailer);
		return"retailerLogin";
	}
}








	


