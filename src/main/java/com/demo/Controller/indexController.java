package com.demo.Controller;

import java.io.File;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;

import com.demo.Repository.ProductRepository;
import com.demo.model.Farmer;
import com.demo.model.Product;

import jakarta.servlet.http.HttpSession;

@Controller
public class indexController {
	
	@Autowired
	private ProductRepository productRepo;
	
	@GetMapping("/")
	public String IndexController() {
		
		return"index";
	}
	
	@GetMapping("/indexproduct")
	public String getIndexProducts(Model model,@ModelAttribute Product product) {
	    File dir = new File("src/main/resources/static/productImages");
	    String[] imgname = dir.list();
	    model.addAttribute("imglist", imgname);
	    List<Product> products = productRepo.findAll(); 
//	    
	    for(Product productss : products) {
//	   
	    model.addAttribute("productList", productRepo.findAll());
////    
	    

////
////
   //String[] productes = new String[2];
   // productes[0] = "products";
   // productes[1] = "productDeatils";
   // System.out.println( productes[1] = "productDeatils");
	    
//	    model.addAttribute("productList", productRepo.findAll());
//    String[] productes = new String[2];
//	    productes[0] = "products";
//    productes[1] = "productDeatils";
    }
	    
        return "products";
		}

	
	
	
	
//	
//	@GetMapping("/indexproduct")
//	public String getIndexProducts(Model model) {
//	    File dir = new File("src/main/resources/static/productImages");
//	    String[] imgname = dir.list();
//	    model.addAttribute("imglist", imgname);
//	    model.addAttribute("productList", productRepo.findAll());
//
//	    String[] productes = new String[2];
//	    productes[0] = "products";
//	    productes[1] = "productDeatils";
//	    model.addAttribute("productes", productes);
//
//	    return "products";
//	}
//
//	
//	
	
	
//
//	@GetMapping("/indexproduct")
//	public String[] getIndexProducts(Model model) {
//	    File dir = new File("src/main/resources/static/productImages");
//	String[] imgname = dir.list();
//	    model.addAttribute("imglist", imgname);
//	 model.addAttribute("productList", productRepo.findAll());
//	    String[] products = new String[2];
//	    products[0] = "products";
//	    products[1] = "productDeatils";
//	    return products;
//	}
	
	
	
	
	
	
	
	
	
//
//	
//	@GetMapping("/details")
//	public String showDetails() {
//		return "productDeatils";
//	}
//		
//	@GetMapping("/details")
//	public String showDetails(Model model,@ModelAttribute Product product) {
//	    File dir = new File("src/main/resources/static/productImages");
//	    String[] imgname = dir.list();
//	    model.addAttribute("imglist", imgname);
//	      model.addAttribute("name",product.getFarm());
//	
//		model.addAttribute("price",product.getCost());
//       return "productDeatils";
//		}
	
	
//	
	@GetMapping("/details")
	public String showDetails(Model model, @ModelAttribute Product product) {
	    File dir = new File("src/main/resources/static/productImages");
	    String[] imgname = dir.list();
	    model.addAttribute("imglist", imgname);
	    List<Product> products = productRepo.findAll(); 
	    
	    
	    model.addAttribute("productList", productRepo.findAll());
		 
	   
	    return "productDeatils";
	}	

	
	
	
	
	
	

	
	
	
	
	
	
	
	
	@GetMapping("/showheader")
	public String showHeader() {
		
		return"bootstrapheader";
	}
	
	
	@GetMapping("/logout")
	public String doLogout(HttpSession session) {
		session.invalidate();
		return"index";
	}

}
