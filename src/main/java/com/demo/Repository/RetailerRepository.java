package com.demo.Repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.demo.model.Retailer;

public interface RetailerRepository extends JpaRepository<Retailer, Integer> {
	


	
		Retailer findByUsernameAndPassword(String un,String psw);
}
